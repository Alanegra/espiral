

function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /* Exit the function: */
      return;
    }
  }
}

includeHTML();

/* Dark mode */
function toggleAll(){
  toggleBody();
  toggleRow();
  togglesfooter();
  togglesnavbar();
  togglesnavli();
  changeImage();
  changeImage2();
  toggles123();
  code();
}

function toggleBody() {
   var element = document.body;
   element.classList.toggle("dark-mode");
}

function togglesnavbar() {
   var x = document.getElementsByTagName("nav");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode3");
   }
}
function toggles123() {
   var x = document.getElementsByClassName("toggle");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode3");
   }
}
function togglesnavli() {
   var x = document.getElementsByTagName("li");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode4");
   }
}

function code() {
   var x = document.getElementsByTagName("pre");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode8");
   }
}


function togglesfooter() {
   var x = document.getElementsByClassName("footer container");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode2");
   }
}
function toggleRow() {
   var x = document.getElementsByClassName("column");
   var i;
   for (i = 0; i < x.length; i++) {
     x[i].classList.toggle("dark-mode6");
   }
}


function changeImage() {
  var image = document.getElementById('myImage');
  if (image.src.match("images/tra-s-w.png")) {
    image.src = "images/tra-s-w.png";
  } else {
    image.src = "images/tra-s-w.png";
  }
}

function changeImage2() {
  var image = document.getElementById('myImage2');
  if (image.src.match("images/tra-s-w.png")) {
    image.src = "images/tra-s-b.png";
  } else {
    image.src = "images/tra-s-w.png";
  }
}





  /*function togglemenu(){
  var menu = document.getElementsById("menu");
  console.log("hola");
  menu.classList.toggleClass("active");
}*/


/* Navbar toggle mobilephone */
function togglemenu() {
  if (document.body.offsetWidth<850){
  var x = document.getElementById("lol");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
}
}
function mostrarNav(){
  if (document.body.offsetWidth>850){
    document.getElementById("lol").style.display="block";
  }
}



/* Gallery  */
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}

/*document.getElementById("menu").addEventListener("click", togglemenu, true);*/
